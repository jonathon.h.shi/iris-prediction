from application import db
import datetime as dt
from sqlalchemy.orm import validates 

class Entry(db.Model):
    __tablename__ = 'predictions'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    sepal_length = db.Column(db.Float)
    sepal_width = db.Column(db.Float)
    petal_length = db.Column(db.Float)
    petal_width = db.Column(db.Float)
    prediction = db.Column(db.Integer)
    predicted_on = db.Column(db.DateTime, nullable=False)

    @validates('sepal_length') 
    def validate_sepal_length(self, key, sepal_length):
        if sepal_length <=0:
            raise AssertionError('Value must be positive')
        return sepal_length

    @validates('sepal_width') 
    def validate_sepal_width(self, key, sepal_width):
        if sepal_width <=0:
            raise AssertionError('Value must be positive')
        return sepal_width
    
    @validates('petal_length') 
    def validate_petal_length(self, key, petal_length):
        if petal_length <=0:
            raise AssertionError('Value must be positive')
        return petal_length

    @validates('petal_width') 
    def validate_petal_width(self, key, petal_width):
        if petal_width <=0:
            raise AssertionError('Value must be positive')
        return petal_width

    @validates('prediction') 
    def validate_prediction(self, key, prediction):
        if prediction < 0:
            raise AssertionError('Value must not be negative')
        return prediction