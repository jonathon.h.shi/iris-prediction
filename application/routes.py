from application import app, ai_model, db
from application.models import Entry
from datetime import datetime
from application.forms import PredictionForm
from flask import render_template, request, flash, json, jsonify

#Global list for look up
iris_type = ['Iris Setosa','Iris Versicolour','Iris Virginica']
#create the database if not exist
db.create_all()

#Handles http://127.0.0.1:5000/
@app.route('/') 
@app.route('/index') 
@app.route('/home') 
def index_page(): 
    form = PredictionForm()
    return render_template("index.html", 
        title="Enter Iris Parameters",
        form=form, entries=get_entries(), iris_type = iris_type)

#Handles http://127.0.0.1:500/predict
@app.route("/predict", methods=['GET','POST'])
def predict():
    form = PredictionForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            sepal_l = form.sepal_l.data
            sepal_w = form.sepal_w.data
            petal_l = form.petal_l.data
            petal_w = form.petal_w.data
            X = [[ sepal_l, sepal_w,  petal_l,  petal_w]]
            result = ai_model.predict(X)
            prob = ai_model.predict_proba(X)
            new_entry = Entry(  sepal_length=sepal_l, sepal_width=sepal_w,
                                petal_length=petal_l,petal_width=petal_w,
                                prediction=int(result[0]*100), 
                                predicted_on=datetime.utcnow())
            add_entry(new_entry)
            flash(f"Prediction: {iris_type[result[0]]} Probability: {prob[0][result[0]]:.2f}%","success")
        else:
            flash("Error, cannot proceed with prediction","danger")
    return render_template("index.html", 
        title="Enter Iris Parameters", 
        form=form, index=True , entries=get_entries(), iris_type = iris_type)

#Handles http://127.0.0.1:5000/remove
@app.route('/remove', methods=['POST'])
def remove():
    form = PredictionForm()
    req = request.form
    id = req["id"]
    remove_entry(id)
    return render_template("index.html", 
        title="Enter Iris Parameters", 
        form=form, entries = get_entries(), iris_type = iris_type, index=True )

#Handles http://127.0.0.1:5000/hello
@app.route('/hello') 
def hello_world(): 
    return "<h1>Hello World</h1>"

#database operation
def add_entry(new_entry):
    try:
        db.session.add(new_entry)
        db.session.commit()
        return new_entry.id
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

def get_entry(id):
    try:
        entries = Entry.query.filter(Entry.id==id)
        result = entries[0]
        return result
    except Exception as error:
        db.session.rollback()
        flash(error,"danger") 
        return 0  

def get_entries():
    try:
        entries = Entry.query.all()
        return entries
    except Exception as error:
        db.session.rollback()
        flash(error,"danger") 
        return 0   

def remove_entry(id):
    try:
        entry = Entry.query.get(id)
        db.session.delete(entry)
        db.session.commit()
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

#API: add entry
@app.route("/api/add", methods=['POST'])
def api_add(): 

    #retrieve the json file posted from client
    data = request.get_json()
    #retrieve each field from the data
    sepal_l     = data['sepal_l']
    sepal_w     = data['sepal_w']
    petal_l     = data['petal_l']
    petal_w     = data['petal_w']
    prediction  = data['prediction']
    #create an Entry object store all data for db action
    new_entry = Entry(  sepal_length=sepal_l, sepal_width=sepal_w,
                        petal_length=petal_l,petal_width=petal_w,
                        prediction = prediction,  
                        predicted_on=datetime.utcnow())
    #invoke the add entry function to add entry                        
    result = add_entry(new_entry)
    #return the result of the db action
    return jsonify({'id':result})

#API get entry
@app.route("/api/get/<id>", methods=['GET'])
def api_get(id): 
    #retrieve the entry using id from client
    entry = get_entry(int(id))
    #Prepare a dictionary for json conversion
    data = {'id'        : entry.id,
            'sepal_l'   : entry.sepal_length, 
            'sepal_w'   : entry.sepal_width,
            'petal_l'   : entry.petal_length,
            'petal_w'   : entry.petal_width,
            'prediction': entry.prediction}
    #Convert the data to json
    result = jsonify(data)
    return result #response back

#API delete entry
@app.route("/api/delete/<id>", methods=['GET'])
def api_delete(id): 
    entry = remove_entry(int(id))
    return jsonify({'result':'ok'})