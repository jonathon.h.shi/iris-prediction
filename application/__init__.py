from flask import Flask
import joblib
#For persistent storage
from flask_sqlalchemy import SQLAlchemy
from flask_heroku import Heroku
import os

#create the Flask app
app = Flask(__name__)

if "TESTING" in os.environ:
    app.config.from_envvar('TESTING')
    print("Using config for TESTING")
elif "DEVELOPMENT" in os.environ:
    app.config.from_envvar('DEVELOPMENT')
    print("Using config for DEVELOPMENT")
else:
    app.config.from_pyfile('config_dply.cfg')
    print("Using config for deployment")
    
# instantiate the Heroku object before db
heroku = Heroku(app)
# instantiate SQLAlchemy to handle db process
db = SQLAlchemy(app)

#AI model file
joblib_file = "./application/static/joblib_NBmodel.pkl"
# Load from file
ai_model = joblib.load(joblib_file)

#run the file routes.py
from application import routes