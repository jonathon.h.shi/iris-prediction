#1: Import libraries need for the test
from application.models import Entry
import datetime as datetime
import pytest
from flask import json

#Unit Test
#2: Parametrize section contains the dara for the test
@pytest.mark.parametrize("entrylist",[
    [  1,  2,  1,  1, 0],  #Test integer arguments
    [0.1,0.2,0.3,0.4, 1]   #Test float arguments
])
#3: Write the test function pass in the arguments
def test_EntryClass(entrylist,capsys):
    with capsys.disabled():
        print(entrylist)
        now = datetime.datetime.utcnow()
        new_entry = Entry(  sepal_length= entrylist[0], 
                            sepal_width = entrylist[1],
                            petal_length= entrylist[2],
                            petal_width = entrylist[3],
                            prediction  = entrylist[4],  
                            predicted_on= now) 

        assert new_entry.sepal_length   == entrylist[0]
        assert new_entry.sepal_width    == entrylist[1]
        assert new_entry.petal_length   == entrylist[2]
        assert new_entry.petal_width    == entrylist[3]
        assert new_entry.prediction     == entrylist[4]
        assert new_entry.predicted_on   == now

@pytest.mark.xfail(reason="arguments <= 0")
@pytest.mark.parametrize("entrylist",[
    [ 0, 2, 1, 1, 0],
    [ 1,-2, 2, 2, 1],
    [ 1, 2,-2, 2, 2],
    [ 1, 2, 2,-2, 1],
    [ 1, 2, 2, 2,-1],
])
def test_EntryValidation(entrylist,capsys):
    test_EntryClass(entrylist,capsys)

#Test add API
@pytest.mark.parametrize("entrylist",[
    [  1,  2,  1,  1,  0],
    [0.1,0.2,0.3,0.4,0.5]
])
def test_addAPI(client,entrylist,capsys):
    with capsys.disabled():
        #prepare the data into a dictionary
        data1 = {   'sepal_l': entrylist[0], 
                    'sepal_w' : entrylist[1],
                    'petal_l': entrylist[2],
                    'petal_w' : entrylist[3],
                    'prediction'  : entrylist[4]}
        #use client object  to post
        #data is converted to json
        #posting content is specified
        response = client.post('/api/add', 
            data=json.dumps(data1),
            content_type="application/json",)
        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        assert response_body["id"]
        
#Test get API
@pytest.mark.parametrize("entrylist",[
    [  1,  2,  1,  1,  0, 1],
    [0.1,0.2,0.3,0.4,0.5, 2]
])
def test_getAPI(client,entrylist,capsys):
    with capsys.disabled():
        response = client.get(f'/api/get/{entrylist[5]}')
        ret = json.loads(response.get_data(as_text=True))
        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        assert response_body["id"] == entrylist[5]
        assert response_body["sepal_l"] == float(entrylist[0])
        assert response_body["sepal_w"] == float(entrylist[1])
        assert response_body["petal_l"] == float(entrylist[2])
        assert response_body["petal_w"] == float(entrylist[3])
        assert response_body["prediction"] == float(entrylist[4])  

#Test delete API
@pytest.mark.parametrize("ind",[1,2])
def test_deleteAPI(client,ind,capsys):
    with capsys.disabled():
        response = client.get(f'/api/delete/{ind}')
        ret = json.loads(response.get_data(as_text=True))
        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"
        response_body = json.loads(response.get_data(as_text=True))
        assert response_body["result"] == "ok"   